# Release Post X.Y MVP Nominations

## Overview

As documented in the handbook the release post team will collect nominations for the release MVP.

## Instructions

- Add a comment to this issue with the contributor name, GitLab profile and some details of their contribution.
- Add 👍 to show support for a contributor named
