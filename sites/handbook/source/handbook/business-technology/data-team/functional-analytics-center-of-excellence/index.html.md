---
layout: handbook-page-toc
title: "Functional Analytics Center of Excellence"
description: "The FACE is a cross-functional group of functional analytics teams that aims to make our teams' more efficient by solving and validating shared data questions which results in cohesive measurement approaches across teams."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Welcome to the FACE of Data Handbook 

**The FACE (functional analytics center of excellence) is a cross-functional group of functional analytics teams that aims to make our teams' more efficient by solving and validating shared data questions which results in cohesive measurement approaches across teams.** 
{: .alert .alert-success}

## Context: why have a functional analytics center of excellence?
The [core data team](https://about.gitlab.com/handbook/business-technology/data-team/) serves as the hub for all of our [“spoke” functional analytical teams](https://about.gitlab.com/handbook/business-technology/data-team/#how-data-works-at-gitlab); however, we have an opportunity to establish spokes between the spokes.

## Objectives: what do we hope to achieve?
- **Efficiency**: it is not uncommon for our teams to be asked similiar questions (eg how are trials converting) but it is inefficient and duplicative for each team to tackle these questions on our own.
- **Alignment**: in cases where we are tackling similiar questions, we need alignment of the assumptions and methodology we are using to answer those questions. The FACE ensures we all deliver cohesive data stories.
- **Knowledge Share**: this forum will give us a formal venue to learn from one another as opposed waiting for organic moments of knowledge sharing.

## Outputs
- **Cadence**: our teams will meet at least once every month if not more. We will knowledge share, align on joint quarerly projects, and develop/prioritize joint asks we have of the core data team. We will also have fun #StaySpoke.
- **Consolidated asks to core data team**: we have an opportunity to streamline, consolidate, and prioritize our asks to the core data team. We also partner with the core data team on data program-level evaluations and decisions (eg BI tooling).
- **Subject Matter Expert Lookup**: we will develop a documented list of people and their associated areas of expertise. SME's will also be able to document their source of truth resources (eg snippets, dashboards, reports).
- **Quarterly Projects**: we will propose and pick cross-functional projects for us to work on quarterly. This will make us all more efficient by assigning 1 DRI with 2 or more code reviewers. 
- **Peer Review + Assumptions Approval**: as a part of the quarterly projects, we will establish a code peer review and assumptions approval process that will ensure we will all be enthusiastic adopters of the ultimate output.
- **Code Repo**: after a project has gone through the peer review and assumptions approval process, we will commit it to a repo that any data person can leverage in their work.

## FACE Teams
|  **Team Name** | **Lead(s)** | 
| :--------------- | :----------------- |
| Product Analytics | Carolyn Braza |
| Business Analytics & Insights | Sindhu Tatimatla |
| Marketing Analytics | Jerome Ahye |
| Self-Service & Online Sales | Alex Martin |
| Sales Analytics | Melia Vilain & Noel Figuera |
| Customer Success Analytics | Michael Arntz |
| Digital Experience | TBH |
| Core Data Team | Marcus Laanen |

## Examples: what are the types problems we tackle together?
- How do free and trial sign-ups convert?
- How do we link namespaces to Salesforce accounts?
- How do we link leads/INQ to Salesforce accounts?
- How do we identify business emails v. junk account emails?


## Workign With Us
- Slack channel: #functional_anlytics_center_of_excellence







