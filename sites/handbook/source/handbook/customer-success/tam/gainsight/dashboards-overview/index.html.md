---
layout: handbook-page-toc
title: "TAM Dashboard Overview"
description: "An overview of the key TAM dashboards."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

*For an overview of Gainsight, and information about how to login, please refer to the [Gainsight Overview Page](/handbook/sales/gainsight/).*

For an overview of how TAMs use Gainsight, please refer to the [Gainsight TAM Overview Page](/handbook/customer-success/tam/gainsight)

## Gainsight dashboard

When logging in, TAMs will begin on their dashboard. Gainsight's homepage will show a "TAM Portfolio" Dashboard which is a full list and overview of each TAM's customers. This dashboard defaults to show the book of business for the current user but can be filtered and configured in a variety of ways to view the information for specific TAMs, teams, etc.

This dashboard is likely to be a TAM's most-frequented page, as it shows a summary of their entire book of business, including last activity dates, health scores, at-risk customers, [upcoming renewals](/handbook/customer-success/tam/renewals/), stage adoption statistics, success plan progress, and more.

There are also other dashboards available to TAMs to look at, including the CS Leadership Dashboard which reviews aspects of all TAM-supported accounts, the Customer Onboarding Dashboard which shows the status of onboarding across all accounts, and the Stage Adoption Dashboard which goes into detail regarding which stages have been adopted and the success of adoption plays.

If there is an asterisk (*) in the title of a report, widget, or field, that means it takes up to 24 hours for that data to update if you change it. For example, in the TAM Burndown Dashboard, there's a widget called `*PR1 Cadence >30 Days`, which shows how many Priority 1 accounts have not had a cadence call recorded in more than 30 days. If you then record a call in Timeline, it will take up to 24 hours for that widget to reflect the updated information.  

When hovering on the far left of the screen, a sidebar will pop up that has a few options:

- Home - you're here!
- Timeline - this [Timeline](/handbook/customer-success/tam/gainsight/timeline/#timeline-view) view shows all customers, rather than an individual customer which can be found on a [Customer 360](/handbook/customer-success/tam/gainsight/#customer-360).
- Cockpit - the Cockpit is where [CTAs](/handbook/customer-success/tam/gainsight/ctas/) live; this Cockpit view shows all CTAs for all of your customers, including success plan objectives.

#### Further detail on specific dashboards
1. [TAM Burn-Down Dashboard](/handbook/customer-success/tam/gainsight/dashboards/#tam-burn-down-dashboard)
1. [TAM Proactive Dashboard](/handbook/customer-success/tam/gainsight/dashboards/#tam-proactive-dashboard)
1. [TAM Key Metrics Dashboard](/handbook/customer-success/tam/gainsight/dashboards/#tam-key-metrics-dashboard)


