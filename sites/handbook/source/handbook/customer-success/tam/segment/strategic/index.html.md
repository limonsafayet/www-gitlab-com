---
layout: handbook-page-toc
title: "TAM Segment: Strategic"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and is not yet final. This page will be updated on an ongoing basis during this phase.
{: .alert .alert-warning}

## Overview

The Strategic segment focuses on our largest customers, as defined by our [segment criteria](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/Segments).

Definition: Named TAM on the account, enabling long-term strategic goals and 1:1 TAM-to-customer enablement/expansion strategies.

Iteration 0 - High-Touch TAM Customer Lifecycle Journey

<img src="/images/handbook/customer-success/ht-tam-customer-journey.png">

[Source](https://lucid.app/lucidchart/4fcdb329-8367-44e0-a1c4-d1c0fd1c2362/edit?invitationId=inv_ab8b15ca-1360-4f53-ab72-1da8e092b620&page=0_0#)

[Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/80)

## Motions

### Align

[Success Plans](handbook/customer-success/tam/success-plans/): The success plan ensures that we strategically engage around customer platform value and not features & functions. This focus ensures that we are spending time on ensuring customer success and growth while defending customer retention.

[Onboarding](handbook/customer-success/tam/onboarding/): Get the customer to first value as soon as possible through primary use-case enablement. Set expectations for the TAM-customer relationship and establish regular check-in calls.

#### Metrics for Align

|  | Enterprise | Commercial |
|---|---|---|
| [Time to first engage](/handbook/customer-success/tam/onboarding/#time-to-engage) | 14 days |  |
| [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value) | 30 days |  |
| [Time to Onboard](/handbook/customer-success/tam/onboarding/#time-to-onboard) | 45 days |  |
| Management-qualified success plans | 100% of accounts |  |

### Enable

The primary objective in enabling our customers is to get a customer to value quickly, ensure any roadblocks to adoption are removed and get the customer to a state of maturity in their desired use cases to provide platform value early on. Continue to drive adoption on primary use cases beyond first value through workshops and webinars. Explore opportunities to increase maturity in primary use cases through more advanced workshops.

#### Metrics for Enable

1. Stage Enablement Playbooks Completed (QoQ)
1. [EBRs](/handbook/customer-success/tam/ebr/):  These strategic touchpoints ensure that progress against desired business outcomes is being made and communicated back to the team that we work with and key influencers/decision-makers. It also creates a vehicle for introducing senior leadership into the customer account, enabling more strategic touchpoints. EBRs completed within the last twelve months in 75% of all accounts.
1. FY23 Metric: Use Case/Stage Adoption Maturity Score: As we begin to have product analytics, we can score the platform adoption per use cases against our best practice benchmarks for mature product adoption, and proactively address areas for improvement.  In FY23 we will prioritize the maturity scores for CI and for DevSecOps, in-line with our Big Rock of ['Expertise in driving CI and DevSecOps adoption & expansion'](https://about.gitlab.com/handbook/customer-success/tam/#big-rock-2-expertise-in-driving-ci-and-devsecops-adoption--expansion) This today is done through cadence calls and qualitative conversations in the absence of maturity analytics.

### Expand & Renew

This phase is about going beyond a customer’s existing use cases, into additional adoption/expansion and tier upgrades.  In ensuring a customer gets to value quickly, in understanding a customer’s desired business outcomes, and in engaging strategically through touchpoints such as EBRs in addition to cadence calls, a TAM is ‘given permission’ as a trusted advisor to introduce and advocate for the idea of growth.

#### Metrics for Expand & Renew

1. Stage Expansion Playbooks Completed (QoQ)
1. Days per Playbook Completed per Stage (QoQ)
1. Win rate for expansion playbooks (QoQ)
