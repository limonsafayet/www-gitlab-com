---
layout: handbook-page-toc
title: "Legal & Corporate Affairs"
description: "Information regarding the Legal & Corporate Affairs Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# Welcome to the Legal & Corporate Affairs Handbook

- This handbook focuses on information related to how to engage the team, as well as information regarding GitLab policies and compliance. 
- We are led by Chief Legal Officer and Corporate Secretary [Robin Schulman](https://ir.gitlab.com/management/robin-schulman)

# Trading Window Status
GitLab Team Members can view the current trading window status via the [Trading Window Status Issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12780) 

## How to Reach Us

#### Anonymous Internal Ethics and Compliance Reporting
We take employee concerns very seriously and encourage all GitLab Team Members to report any ethics and/or compliance violations by using EthicsPoint. Further details are found on the [People Ops Handbook page](https://about.gitlab.com/handbook/people-group/#how-to-report-violations) and in our [Code of Business Conduct and Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/).

#### Quick Questions
For quick questions that **_do not_** require legal advice, deliverables, or any discussion of confidential information, you can reach out to the GitLab Legal & Corporate Affairs Team at *[`#legal`](https://gitlab.slack.com/archives/legal)*. We find this channel best for questions regarding process, who handles what or how to find certain things if the Handbook has not yielded the right result for you after searching. *[`#legal`](https://gitlab.slack.com/archives/legal)* is not a private channel, so your inquiry will be visible to the entire company. One of our Team Members will do their best to answer your question in a timely fashion. **If your request is sales or procurement related, involves legal advice, deliverables, or any discussion of confidential information, please follow the process set forth in the table below.** 


| Type of Request | Process / Information |
| ------ | ------ |
| Sales / Customer Related matters (which includes any matter that is, or could potentially be, revenue generating) | [Sales Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/customer-negotiations/)|
| Procurement / Vendor (which includes any matters related to a potential purchase by GitLab) | [Procurement Page](https://about.gitlab.com/handbook/finance/procurement/) Procurement will engage legal as necessary <br /><br />Visit the [Procurement Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/procurement-guide-collaborating-with-gitlab-legal/) for additional information |
| Marketing | Visit the [Marketing Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/marketing-collaboration/), which covers amongst other things [Contests and Sweepstakes](https://about.gitlab.com/handbook/legal/marketing-collaboration/#contests-and-sweepstakes) and [Media Consent and Model Release Requests](https://about.gitlab.com/handbook/legal/marketing-collaboration/#media-consent-and-model-release)<br /><br />If the request is not addressed in the Marketing Guide, open a new [General Legal Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=general-legal-template)|
| Engineering requests related to use of third-party software; Questions related to acceptable use of open source licenses | Open a [Legal Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues) and include applicable information related to the request<br /><br />Visit [Open Source at GitLab](https://about.gitlab.com/handbook/engineering/open-source/#using-open-source-libraries) for helpful information including [Acceptable Licenses](https://about.gitlab.com/handbook/engineering/open-source/#acceptable-licenses)|
| *Any Non Engineering, Marketing, Sales or Procurement Request (Corporate, Marketing, Employment or Product & Privacy Questions) | Email: [Legal_Internal@gitlab.com](mailto:legal_internal@gitlab.com) |
| NDA Request | Follow the [Non-Disclosure Agreement Process](https://about.gitlab.com/handbook/legal/NDA/) |

*To ensure the protection of confidential and privileged information, GitLab Legal is proud to use [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) for all general requests.
For more information on Attorney-Client Privilege, see the [General Legal FAQs](https://about.gitlab.com/handbook/legal/#general-legal-faqs) below.  

## Policies and Guidelines
* [Anti-Corruption Policy](https://about.gitlab.com/handbook/legal/anti-corruption-policy/) - this policy provides guidance regarding ethical business conduct, applicable laws, rules, and regulations.
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [Compliance](https://about.gitlab.com/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Corporate Communications Policy](https://about.gitlab.com/handbook/legal/corporate-communications/) - GitLab's policy for how to handle Company communications. 
* [Corporate Governance Guidlines](https://ir.gitlab.com/static-files/f108c2c0-2426-4bf5-b1db-054498136c0b) - guidlines to assist the Board of Directors in the exercise of its governance reponsibilities and serve as framework within which the Board of Directors may conduct its business.  
* [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/) GitLab's policy for how it handles personal information about team members.
* [General Guidelines](https://about.gitlab.com/handbook/general-guidelines/) - general company guidelines
* [GitLab Modern Slavery Act Transparency Statement](https://about.gitlab.com/handbook/legal/modern-slavery-act-transparency-statement/)
* [Insider Trading Policy](https://drive.google.com/drive/folders/1kB3k5FRnR3OUBP0Eyo3SxxyPKeiRFfUk) - this policy is to promote compliance with insider trading laws.
* [Merger and Acquisitions Committee Charter](https://drive.google.com/file/d/1_GG0JJ2NUq50djVx-yxwnrRJUhIYhs3D/view?usp=sharing) - Gitlab's Mergers and Acquisitions Committee Charter.
* [Patent Program](https://about.gitlab.com/handbook/legal/patent-program/)
* [Privacy Policy](https://about.gitlab.com/privacy/) - GitLab's policy for how it handles personal information
* [Public Discussion of Competitor Products Guidelines](handbook/legal/competitor-discussion-guidelines)
* [Records Retention Policy](https://about.gitlab.com/handbook/legal/record-retention-policy/) - GitLab's policy on the implementation of procedures, best practices, and tools to promote consistent life cycle management of GitLab records
* [Related Party Transactions Policy](https://about.gitlab.com/handbook/legal/gitlab-related-party-transactions-policy/) - this Policy formalizes GitLab's procedures for the identification, review, and consideration and approval of any transactions involving the Company and a Related Party. 
* [Trademark](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-standards/#trademark) - information regarding the usage of GitLab's trademark
* [Use of Third-party Trademarks in GitLab](https://about.gitlab.com/handbook/legal/policies/product-third-party-trademarks-guidelines/) - guidelines concerning the use of third-party trademarks in the GitLab product
* [Whistleblower Policy](https://drive.google.com/drive/folders/1kB3k5FRnR3OUBP0Eyo3SxxyPKeiRFfUk) - this policy is provided to enable Team Members with guidelines on on how to report any improper activites, or suspected improper activities. 


_NOTE: The Insider Trading Policy, Mergers and Acquisitions Committee Charter and Whistleblower Policy are only accessible to GitLab Team Members signed into their GitLab Account_


## Helpful Resources 

* For information related to compliance, including the Insider Trading Policy & Insider Trading Policy FAQ, and various Designated Insider information please visit the [Compliance Information Handbook Page](https://about.gitlab.com/handbook/legal/publiccompanyresources/) 
* Compliance trainings for team members are provided by GitLab Legal via [GitLab Learn](https://gitlab.edcast.com/) and [Navex](https://lms.navexglobal.com/topclass5/).
* [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
* [Requirements for Closing Deals](https://about.gitlab.com/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) - requirements for closing sales deals
* [Terms](https://about.gitlab.com/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Acceptable Licenses](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/licensing.md) - list of acceptable licenses covering third party components used in development
* [Trademarks training materials](https://about.gitlab.com/handbook/legal/trademarks-training-materials), covering _An Introduction to Trademarks_, _Using Third-party Trademarks_, and _Using the GitLab Trademark_.
* [Legal Tech Stack](https://docs.google.com/spreadsheets/d/1nLtWVx6mebR7_y2Qv_CcScbVW-ryLVzvcFVgGk2yeRs/edit#gid=686289913)- overview of tools used throughout the GitLab Legal Team. _NOTE: This document is only available to GitLab Legal Team Members_

## Processes
* [Uploading Third Party Contracts to ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/)
* [Issue Tracker Workflows](https://about.gitlab.com/handbook/legal/issue-tracker-workflows/)
* [Privacy Review Process](https://about.gitlab.com/handbook/legal/privacy/#privacy-review-process)

# Code of Business Conduct and Ethics 
GitLab's Code of Business Conduct and Ethics summarizes the ethical standards for all directors, officers, team members and contractors of GitLab and of its direct and indirect subsidiaries and is a reminder of the seriousness of our commitment to our values. [Please click here to read GitLab's Code of Business Conduct and Ethics in its entirety.](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d) 

# General Legal FAQs

## The U.S. Attorney-Client Relationship
This discussion is limited to the practice of law in the U.S. As we continue to grow globally, we will update this and expand how privilege applies in other jurisdictions.

<details>
 <summary markdown="span">What is Attorney-Client Privilege?</summary>
Attorney-Client Privilege is a principle that provides protections for certain communications between clients and their attorneys that meet specific criteria. First of all, the communications must be for the purpose of seeking legal guidance and advice. For this reason, the underlying facts may not be protected if they are available from another source. Opinions and analysis of the facts, and discussions thereof, with the attorney are protected. Secondly, communications must be confidential. Information is also not protected if it is available from another source meaning that simply telling your attorney or copying your attorney on a communication does not protect the information.
</details>

<details>
 <summary markdown="span">What is Work Product Privilege?</summary>
Work Product is a U.S. doctrine in which an attorney’s notes, observations, thoughts, and research prepared by, or at the direction of, an attorney in anticipation of litigation are protected from being discoverable during the litigation process.
</details>

<details>
 <summary markdown="span">What is the purpose of these Privileges?</summary>
Attorney-Client and Work Product privileges allow clients to speak freely with their attorneys and encourage full disclosure so they can receive accurate and competent legal advice without the fear of having their attorney compelled to testify against them and disclose the information shared by the client.
</details>

<details>
 <summary markdown="span">Who do these Privileges apply to at GitLab?</summary>
There is not one uniform answer that covers all jurisdictions in the U.S. However, most jurisdictions will use at least one of the following tests to see if the individuals involved have privileged communication.
<br>- The Control Group Test is quite restrictive and only allows for the protection of corporate communications to the corporation's controlling executives and managers. This test cannot be used in federal courts, but is still used in some states.</br>
<br>- Instead of looking solely at the roles of the employees involved, the Subject Matter Test looks at the subject matter of the employees’ communications. If an employee has been directed by a supervisor to discuss a subject matter that relates to the employees job with an attorney, this may be covered by subject matter privilege.</br>
<br>- The Upjohn Test is a modified version of the Subject Matter Test requires additional criteria to be met. In addition to the subject matter being relevant to the employee’s duties, the employee must also have awareness and intent concerning the legal advice being sought and/or given.</br>
<br>- A company’s attorney does not represent an employee individually, but instead represents the interests of the company. A company can waive its privilege at any time, meaning the company could choose to disclose information the attorney received from a covered employee in confidence for use as evidence in a legal proceeding in order to protect the company from liability.</br>
</details>

<details>
 <summary markdown="span">How do you protect the Privileges that apply to you when seeking Legal advice?</summary>
Direct the communication to a practicing licensed attorney. Privilege does not apply to other non-attorney members of the Legal Team.
It is best practice to have privileged conversations with the attorney via Zoom.
If other individuals will need to participate in the discussion, consult with the attorney, and only include the minimum necessary individuals in the conversation, in other words, keep the circle of trust small.
If it is necessary to communicate by email, in the “Subject” line, and at the top of the body of the communication, include the phrase “AC PRIV”.
Do not overuse the claim of privilege. Limit its use to when actually seeking legal guidance and advice and not on any and every correspondence with the attorney.
You must keep the information discussed confidential, and not share it with anyone outside the circle of trust without first consulting with the attorney.
Do not forward protected emails to anyone outside the circle of trust.
Do not copy anyone outside the circle of trust on emails with the attorney.
</details>

For more questions and answers about Attorney-Client Privilege in the corporate setting, search “AC Priv tests” in Drive.

## Litigation Holds

<details>
 <summary markdown="span">What is a litigation hold?</summary>
A litigation hold is the process a company uses to preserve all forms of relevant evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to pending or imminent litigation or when litigation is reasonably anticipated. Litigation holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on the company's case, including leading to sanctions. Once the company becomes aware of potential litigation, the company's attorney will provide notice to the impacted employees, instructing them not to delete or destroy any information relating to the subject matter of the litigation. The litigation hold applies to paper and electronic documents. During a litigation hold, all retention policies must be overridden.
</details>

## Foreign Corrupt Practices Act

<details>
 <summary markdown="span">What is the Foreign Corrupt Practices Act?</summary>
The Foreign Corrupt Practices Act (“FCPA”) is a United States federal law that prohibits U.S. citizens and entities from bribing foreign government officials to benefit their business interests. It is not only an invaluable tool to help fight corruption but one to which we must be compliant. As GitLab Inc. is a U.S. incorporated entity, we need to make sure our operations worldwide are compliant with the provisions of the Foreign Corrupt Practices Act. To that end, GitLab requires Team Members to complete an annual online course relating to anti-bribery and corruption at GitLab. In the training, learners will explore improper payments, including facilitation payments and personal safety payments, as well as policies on commercial bribery. The goal of the course is to ensure our Team Members understand what it takes to avoid corruption, especially in high-risk countries, and to ensure GitLab is compliant with legal and regulatory obligations.
</details>
